# HSPC Sandbox Dev
![HSPC Logo](images/hspc-logo-main-375.png)

Welcome to the HSPC Reference Impl project!

This project is used as the root (container) project for the HSPC sandbox development projects.

## What is this repository for? ##

* Ansible installer for gaining all of the HSPC Reference and Sandbox projects
* Maven aggregator project

## What will I have after completing this guide? ##

* All HSPC sourcecode projects as children of the ~/projects/hspc folder (referred to as <HSPC_PROJECT_DIR> in this guide)
* An instance of Apache Tomcat located at ~/servers/apache-tomcat-5.8.9
* The HSPC technology stack deployed to your Apache Tomcat server (http://localhost:8080)
* An IntelliJ project for developing the HSPC technology stack

## How do I get set up? ##

### Prerequisites ###
* Install XCode and the XCode Development Tools
* Install the latest Java Platform SDK (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* Install [Ansible 2.1.0](http://docs.ansible.com/intro_installation.html)

    ```
    sudo pip install ansible==2.1.0
    ```

* Install MySQL
* [Caching your GitHub password in Git](https://help.github.com/articles/caching-your-github-password-in-git/)
* Install GNU-Tar

    ```
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    ...
    brew install gnu-tar
    ```

### Installation ###
* Clone this project to your projects folder <HSPC_PROJECT_DIR> (ex: ~/projects/hspc)
* Create a file, /etc/ansible/hosts, that identifies your development machine as a "devmachine" with a local connection:

    ```
    devmachine ansible_connection=local
    ```

* Speed up local SSH (try to skip this item as "local" connection should not use ssh)

    ```
    sudo vi /etc/ssh/sshd_config
    // change from yes to no (or add it if it's not there)
    UseDNS no
    ```

* Override values in the ansible/settings-override.yml file, adding properties you would like to override:

    ```
    default_depth: "1"
    projects_dir: "{your projects dir}"
    username: "{your username}"
    ```

### Projects, Servers, Deployments ###
All projects, servers, and deployments can be completed using the following command:

    ```
    ansible-playbook <HSPC_PROJECT_DIR>/reference-impl/ansible/site.yml
    ```

### Ansible Playbooks ###
The following Ansible playbooks exist:

| Playbook        | Description            |
| :------------ | :------------- |
| site.yml      | Complete install.  Includes environment, tomcat, projects-installers, projects-installers-code, and projects-installers-build-deploy playbooks. |
| environment.yml      | Creates folders and installs utility software. |
| tomcat.yml | Installs apache-tomcat application server.     |
| projects-installers.yml | Clones the HSPC sandbox development project installers     |
| projects-installers-code.yml | Runs each project installer's playbook to clone the project repositories     |
| projects-installers-build-deploy.yml | Runs each project installer's playbook to build and deploy the project. |
| uninstall.yml | Removes the servers and projects folders for this installer |
| vertical.yml | Similar to site.yml but runs each server lifecycle vertically: code/build/deploy |

## How do I get IntelliJ setup? ##

This project, reference-impl, contains a pom.xml file that uses Maven modules to include all of the HSPC sourcecode projects (as a whole, and as profile groups)*[]:

To open the HSPC sourcecode projects in IntelliJ,

* Open IntelliJ (community or ultimate edition) (https://www.jetbrains.com/idea/)
* Select File - Open
* Select the pom file: <HSPC_PROJECT_DIR>/reference-impl/pom.xml
* Notice that IntelliJ has loaded the reference-impl project's Maven default profile.  Select the IntelliJ Maven Projects tool (on the far-right of the IDE).  Notice that you can choose from a number of defined profiles based on your area of work.  Select the "Complete" profile to get the entire HSPC sourcecode stack within your IDE.

## How do I build a project component and deploy it? ##

### Using Maven directly ###
The codebase uses Maven as the build tool.  You can build a single component (ex: reference-api-mysql) by running "mvn compile" (or either package, install) from the component directory (ex: ~/projects/hspc/reference-api-mysql).  Some components are JARs that are used by deployable components (WARs).  Whole systems (ex: reference-api) can be build using the Maven profiles in the reference-impl project.  For example, the API system can be built by running this command from the <HSPC_PROJECT_DIR> folder:

    ```
    ~/projects/hspc> mvn install -f reference-impl/pom.xml -Papi
    ```

After building a deployable project, you can deploy it to Tomcat by copying it to the Tomcat webapps directory:

    ```
    ~/projects/hspc> cp reference-api-webapp-multitenant/target/*.war ~/servers/apache-tomcat-8.5.9/webapps
    ```

### Using the installer (Ansible) project ###
You can use the installer project to run tasks and playbooks to perform your desired work.  Each task has a few tags defined to allow fine-grained execution.  For example, to build and deploy the reference-api system, the following Ansible command may be used:

    ```
    ~/projects/hspc/reference-impl/ansible> ansible-playbook site.yml --tags[api-build,api-deploy]
    ```

## How do I use the deployed system? ##

The public entry point to the system is the Sandbox Manager application.  It will be running by default at [http://localhost:8000/hspc-sandbox-manager](Link URL).

## What systems are running ##

* [http://localhost:8060/](HSPC Reference Auth)